# My bspwm rice
> **Note**: This rice is in work in progress, setting up pywal everywhere takes some time.

I bought a new computer so I redone my old rice. I'll also provide some snippets to make ricing easier later in another repo.

## Features
* Synchronized colorscheme with the wallpaper using **pywal** on all applications
* Lightweight display manager with **ly**
* Cool fonts with **Nerd Fonts**
* Powerful yet simple sound system with CLI controls, GUI controls and noise suppression with **Pipewire**
* Security with 2-step auth with **libfido2** and **eid** (if you're belgian)
* Fast, light and really nice tiling window manager, shortcut and compositor with **bspwm**, **sxhkd**, **polybar and **picom**
* Secure and private web browser with password manager built in, adblocker, VIM keybindings and dark theme everywhere with **Firefox** (*enhanced with betterfox, ublock origin, dark reader, a custom script with violentmonkey and tridactyl*)
* Communication made easy with **Discord**
* Cool light general purpose softwares (image, document, video, screenshots, terminal emulator) with **nsxiv, mupdf, mpv, vlc, maim** and **alacritty**
* Softwares to make things (documents, images, videos, code) : **onlyoffice**, **krita**, **shotcut**, **nano** and **vscodium**

## Installing the softwares
You can find the install commands in the [install.sh script](./install.sh).

## Configure
My configuration might not be yours, but you can find my configuration files in the `.config` directory. Feel free to copy what you like and add it to yours.
