#!/bin/bash
# This script installs all the software listed on the README page for Arch Linux, assuming you already have a working install

# Installing yay 
cd /tmp
pacman -S --needed git base-devel
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

# Installing ly
cd /tmp
git clone --recurse-submodules https://github.com/fairyglade/ly
cd ly
make
make install installsystemd
systemctl dusable getty@tty2.service

# Audio system
yay -S pipewire wireplumber noisetorch qpwgraph alsa-utils pipewire-pulse pipewire-jack pipewire-alsa

# File archives
yay -S zip unzip

# Password management (nitrokey and eID)
yay -S libfido2 ccid eid-mw

# Brightness control
yay -S brightnessctl

# Pywal
yay -S python python-pip
pip install pywal

# Fonts
mkdir -p ~/.fonts
cd ~/.fonts
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.2/Hermit.zip
unzip Hurmit.zip

# Xorg, Window manager, keyboard shortcuts manager, compositor, bar, app launcher
yay -S xorg xorg-xinit bspwm sxhkd picom polybar rofi

# Web browser, communication, image viewer, document viewer, video viewer, screenshots, terminal emulator
yay -S firefox discord nsxiv mupdf mpv vlc maim xclip alacritty

# Document editor, image editor, video editor, text and code editor
yay -S onlyoffice-bin krita shotcut nano vscodium
